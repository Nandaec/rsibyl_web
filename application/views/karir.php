<section class="wrapper">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="center">Karir</h1>
				<p class="center">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque rutrum pellentesque imperdiet. Nulla lacinia iaculis nulla non metus. pulvinar. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Ut eu risus enim, ut pulvinar lectus. Sed hendrerit nibh.
				</p>
				<hr>
			</div>
		</div>
		<div class="row">
			<div class="col-md-7">
				<!-- <div class="post-image">
					<div class="flexslider" data-plugin-options='{"directionNav":true}'>
						<ul class="slides">
							<li>
								<img src="<?php echo base_url();?>assets/frontend/images/gambar/karir.jpeg" />
							</li>
						</ul>
					</div>
				</div> -->
				<div class="panel-group accordion-custom accordion-teal" id="accordion">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
									<i class="icon-arrow"></i>
									Advertising Sales Rep
								</a>
							</h4>
						</div>
						<div id="collapseOne" class="panel-collapse collapse">
							<div class="panel-body">
								<p>
									<strong>Location:</strong> New York - <strong>Department:</strong> Engineering
								</p>
								<p>
									Mauris elit velit, lobortis sed interdum at, vestibulum vitae libero. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque iaculis ligula ut ipsum mattis viverra.
								</p>
								<p>
									<a href="#" class="btn btn-main-color pull-bottom">
										Apply Now
									</a>
								</p>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
									<i class="icon-arrow"></i>
									Graphic Designer
								</a>
							</h4>
						</div>
						<div id="collapseTwo" class="panel-collapse collapse">
							<div class="panel-body">
								<p>
									<strong>Location:</strong> New York - <strong>Department:</strong> Engineering
								</p>
								<p>
									Mauris elit velit, lobortis sed interdum at, vestibulum vitae libero. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque iaculis ligula ut ipsum mattis viverra.
								</p>
								<p>
									<a href="#" class="btn btn-main-color pull-bottom">
										Apply Now
									</a>
								</p>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
									<i class="icon-arrow"></i>
									Marketing Manager
								</a>
							</h4>
						</div>
						<div id="collapseThree" class="panel-collapse collapse">
							<div class="panel-body">
								<p>
									<strong>Location:</strong> New York - <strong>Department:</strong> Engineering
								</p>
								<p>
									Mauris elit velit, lobortis sed interdum at, vestibulum vitae libero. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque iaculis ligula ut ipsum mattis viverra.
								</p>
								<p>
									<a href="#" class="btn btn-main-color pull-bottom">
										Apply Now
									</a>
								</p>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
									<i class="icon-arrow"></i>
									Web Developer
								</a>
							</h4>
						</div>
						<div id="collapseFour" class="panel-collapse collapse">
							<div class="panel-body">
								<p>
									<strong>Location:</strong> New York - <strong>Department:</strong> Engineering
								</p>
								<p>
									Mauris elit velit, lobortis sed interdum at, vestibulum vitae libero. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque iaculis ligula ut ipsum mattis viverra.
								</p>
								<p>
									<a href="#" class="btn btn-main-color pull-bottom">
										Apply Now
									</a>
								</p>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFifth">
									<i class="icon-arrow"></i>
									Web Master
								</a>
							</h4>
						</div>
						<div id="collapseFifth" class="panel-collapse collapse">
							<div class="panel-body">
								<p>
									<strong>Location:</strong> New York - <strong>Department:</strong> Engineering
								</p>
								<p>
									Mauris elit velit, lobortis sed interdum at, vestibulum vitae libero. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque iaculis ligula ut ipsum mattis viverra.
								</p>
								<p>
									<a href="#" class="btn btn-main-color pull-bottom">
										Apply Now
									</a>
								</p>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
									<i class="icon-arrow"></i>
									Writer and Editor
								</a>
							</h4>
						</div>
						<div id="collapseSix" class="panel-collapse collapse">
							<div class="panel-body">
								<p>
									<strong>Location:</strong> New York - <strong>Department:</strong> Engineering
								</p>
								<p>
									Mauris elit velit, lobortis sed interdum at, vestibulum vitae libero. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque iaculis ligula ut ipsum mattis viverra.
								</p>
								<p>
									<a href="#" class="btn btn-main-color pull-bottom">
										Apply Now
									</a>
								</p>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven">
									<i class="icon-arrow"></i>
									Human Resources
								</a>
							</h4>
						</div>
						<div id="collapseSeven" class="panel-collapse collapse">
							<div class="panel-body">
								<p>
									<strong>Location:</strong> New York - <strong>Department:</strong> Engineering
								</p>
								<p>
									Mauris elit velit, lobortis sed interdum at, vestibulum vitae libero. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque iaculis ligula ut ipsum mattis viverra.
								</p>
								<p>
									<a href="#" class="btn btn-main-color pull-bottom">
										Apply Now
									</a>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-5">
				<h2>Pengumuman</h2>
				<!-- BEGIN FORM-->
				<ul class="nav nav-list blog-categories">
					<li>
						<a target="_blank" href="<?php echo base_url();?>assets/download/test.pdf">
							Peserta Rekrutmen Maret 2020
						</a>
					</li>
				</ul>
				<!-- END FORM-->
			</div>
		</div>
	</div>
</section>