<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Clip-One - Front End</title>
		<!-- start: META -->
		<!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta content="" name="description" />
		<meta content="" name="author" />
		<!-- end: META -->
		<!-- start: MAIN CSS -->
		<link href="<?php echo base_url();?>assets/frontend/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/frontend/plugins/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/frontend/fonts/style.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/frontend/plugins/animate.css/animate.min.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/frontend/css/main.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/frontend/css/main-responsive.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/frontend/css/owl.carousel.min.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/frontend/css/theme_blue.css" type="text/css" id="skin_color">
		<!-- end: MAIN CSS -->
		<!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/frontend/plugins/revolution_slider/rs-plugin/css/settings.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/frontend/plugins/flex-slider/flexslider.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/frontend/plugins/colorbox/example2/colorbox.css">
		<!-- end: HTML5SHIV FOR IE8 -->
	</head>
	<!-- end: HEAD -->
	<body>
		<!-- start: HEADER -->
		<header>
			<!-- start: TOP BAR -->
			<div class="clearfix " id="topbar">
				<div class="container">
					<div class="row">
						<div class="col-sm-6">
							<!-- start: TOP BAR CALL US -->
							<div class="callus">
								Call Us: (641)-734-4763 - Mail:
								<a href="mailto:info@example.com">
									info@example.com
								</a>
							</div>
							<!-- end: TOP BAR CALL US -->
						</div>
						<div class="col-sm-6">
							<!-- start: TOP BAR SOCIAL ICONS -->
							<div class="social-icons">
								<ul>
									<li class="social-twitter tooltips" data-original-title="Twitter" data-placement="bottom">
										<a target="_blank" href="http://www.twitter.com">
											Twitter
										</a>
									</li>
									<li class="social-dribbble tooltips" data-original-title="Dribbble" data-placement="bottom">
										<a target="_blank" href="http://dribbble.com">
											Dribbble
										</a>
									</li>
									<li class="social-facebook tooltips" data-original-title="Facebook" data-placement="bottom">
										<a target="_blank" href="http://facebook.com">
											Facebook
										</a>
									</li>
									<li class="social-google tooltips" data-original-title="Google" data-placement="bottom">
										<a target="_blank" href="http://google.com">
											Google+
										</a>
									</li>
									<li class="social-linkedin tooltips" data-original-title="LinkedIn" data-placement="bottom">
										<a target="_blank" href="http://linkedin.com">
											LinkedIn
										</a>
									</li>
									<li class="social-youtube tooltips" data-original-title="YouTube" data-placement="bottom">
										<a target="_blank" href="http://youtube.com/">
											YouTube
										</a>
									</li>
									<li class="social-rss tooltips" data-original-title="RSS" data-placement="bottom">
										<a target="_blank" href="#" >
											RSS
										</a>
									</li>
								</ul>
							</div>
							<!-- end: TOP BAR SOCIAL ICONS -->
						</div>
					</div>
				</div>
			</div>
			<!-- end: TOP BAR -->
			<div role="navigation" class="navbar navbar-default navbar-fixed-top space-top">
				<!-- start: TOP NAVIGATION CONTAINER -->
				<div class="container">
					<div class="navbar-header">
						<!-- start: RESPONSIVE MENU TOGGLER -->
						<button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<!-- end: RESPONSIVE MENU TOGGLER -->
						<!-- start: LOGO -->
						<a class="navbar-brand" href="index.html">
							<img alt="" src="<?php echo base_url();?>assets/frontend/images/logo/rsiboyolali.png">
						</a>
						<!-- end: LOGO -->
					</div>
					<div class="navbar-collapse collapse">
						<ul class="nav navbar-nav navbar-right">
							<li class="active">
								<a href="<?php echo base_url();?>home">
									Home
								</a>
							</li>
							<li class="dropdown">
								<a class="dropdown-toggle" href="#" data-toggle="dropdown" data-hover="dropdown">
									Profil <b class="caret"></b>
								</a>
								<ul class="dropdown-menu">
									<li>
										<a href="pages_services.html">
											Sejarah
										</a>
									</li>
									<li>
										<a href="pages_services.html">
											Direksi
										</a>
									</li>
									<li>
										<a href="pages_about.html">
											Visi & Misi
										</a>
									</li>
								</ul>
							</li>
							<li class="dropdown">
								<a class="dropdown-toggle" href="#" data-toggle="dropdown" data-hover="dropdown">
									Informasi <b class="caret"></b>
								</a>
								<ul class="dropdown-menu">
									<li>
										<a href="portfolio_example1.html">
											Fasilitas Kamar
										</a>
									</li>
									<li>
										<a href="portfolio_example2.html">
											Alur Pelayanan
										</a>
									</li>
								</ul>
							</li>
							<li>
								<a href="<?php echo base_url();?>karir">
									Karir
								</a>
							</li>
							<li>
								<a href="index.html">
									Kontak
								</a>
							</li>
						</ul>
					</div>
				</div>
				<!-- end: TOP NAVIGATION CONTAINER -->
			</div>
		</header>
		<!-- end: HEADER -->
		<!-- start: MAIN CONTAINER -->
		<div class="main-container">
			<!-- start: REVOLUTION SLIDERS -->
			<?php $this->load->view($page); ?>
		</div>
		<!-- end: MAIN CONTAINER -->
		<!-- start: FOOTER -->
		<footer id="footer">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<div class="newsletter">
							<h4>Lokasi RSU Islam Boyolali</h4>
							<div class="map">
								<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3955.22303223567!2d110.61962661477662!3d-7.550641594554497!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e7a6bd2db736667%3A0xc4560719c4eb7cfd!2sRumah%20Sakit%20Islam%20Boyolali!5e0!3m2!1sen!2sid!4v1607269903736!5m2!1sen!2sid" width="400" height="120" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="contact-details">
							<h4>Contact Us</h4>
							<ul class="contact">
								<li>
									<p>
										<i class="fa fa-map-marker"></i><strong>Address:</strong> 1234 Street Name, City Name, United States
									</p>
								</li>
								<li>
									<p>
										<i class="fa fa-phone"></i><strong>Phone:</strong> (123) 456-7890
									</p>
								</li>
								<li>
									<p>
										<i class="fa fa-envelope"></i><strong>Email:</strong>
										<a href="mailto:mail@example.com">
											mail@example.com
										</a>
									</p>
								</li>
							</ul>
						</div>
					</div>
					<div class="col-md-2">
						<h4>Follow Us</h4>
						<div class="social-icons">
							<ul>
								<li class="social-twitter tooltips" data-original-title="Twitter" data-placement="bottom">
									<a target="_blank" href="http://www.twitter.com">
										Twitter
									</a>
								</li>
								<li class="social-facebook tooltips" data-original-title="Facebook" data-placement="bottom">
									<a target="_blank" href="http://facebook.com" data-original-title="Facebook">
										Facebook
									</a>
								</li>
								<li class="social-linkedin tooltips" data-original-title="LinkedIn" data-placement="bottom">
									<a target="_blank" href="http://linkedin.com" data-original-title="LinkedIn">
										LinkedIn
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="footer-copyright">
				<div class="container">
					<div class="row">
						<div class="col-md-1">
							<a class="logo" href="index.html">
								CLIP<i class="clip-clip"></i>ONE
							</a>
						</div>
						<div class="col-md-7">
							<p>
								&copy; Copyright 2014 by Clip-One. All Rights Reserved.
							</p>
						</div>
						<div class="col-md-4">
							<nav id="sub-menu">
								<ul>
									<li>
										<a href="#">
											FAQ's
										</a>
									</li>
									<li>
										<a href="#">
											Sitemap
										</a>
									</li>
									<li>
										<a href="#">
											Contact
										</a>
									</li>
								</ul>
							</nav>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<a id="scroll-top" href="#"><i class="fa fa-angle-up"></i></a>
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
		<!--<![endif]-->
		<script src="<?php echo base_url();?>assets/frontend/plugins/bootstrap/js/bootstrap.min.js"></script>
		<script src="<?php echo base_url();?>assets/frontend/plugins/jquery.transit/jquery.transit.js"></script>
		<script src="<?php echo base_url();?>assets/frontend/plugins/hover-dropdown/twitter-bootstrap-hover-dropdown.min.js"></script>
		<script src="<?php echo base_url();?>assets/frontend/plugins/jquery.appear/jquery.appear.js"></script>
		<script src="<?php echo base_url();?>assets/frontend/plugins/blockUI/jquery.blockUI.js"></script>
		<script src="<?php echo base_url();?>assets/frontend/plugins/jquery-cookie/jquery.cookie.js"></script>
		<script src="<?php echo base_url();?>assets/frontend/js/main.js"></script>
		<!-- end: MAIN JAVASCRIPTS -->
		<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
		<script src="<?php echo base_url();?>assets/frontend/plugins/revolution_slider/rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
		<script src="<?php echo base_url();?>assets/frontend/plugins/revolution_slider/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
		<script src="<?php echo base_url();?>assets/frontend/plugins/flex-slider/jquery.flexslider.js"></script>
		<script src="<?php echo base_url();?>assets/frontend/plugins/stellar.js/jquery.stellar.min.js"></script>
		<script src="<?php echo base_url();?>assets/frontend/plugins/colorbox/jquery.colorbox-min.js"></script>
		<script src="<?php echo base_url();?>assets/frontend/js/owl.carousel.min.js"></script>
		<script src="<?php echo base_url();?>assets/frontend/js/index.js"></script>
		<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
		<script>
			jQuery(document).ready(function() {
				Main.init();
				Index.init();
				$.stellar();
			});
		</script>
	</body>
</html>
