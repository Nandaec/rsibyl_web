<section class="fullwidthbanner-container">
	<div class="fullwidthabnner">
		<ul>
			<li data-transition="fade" data-slotamount="7" data-masterspeed="1500" >
				<img src="<?php echo base_url();?>assets/frontend/images/sliders/slidebg1.png"  style="background-color:rgb(246, 246, 246)" alt="slidebg1"  data-bgfit="cover" data-bgposition="left bottom" data-bgrepeat="no-repeat">
				<div class="caption lfb"
					data-x="150"
					data-y="-30"
					data-speed="700"
					data-start="1000"
					data-easing="easeOutExpo"  >
					<img style="height: 600px;" src="<?php echo base_url();?>assets/frontend/images/gambar/direksi.png" alt="Image 1">
				</div>
			</li>
		</ul>
	</div>
</section>

<section class="wrapper padding50 animate-group">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="center">Layanan Unggulan</h1>
				<hr>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="feature-box">
					<div class="feature-icon animate" data-animation-options='{"animation":"fadeInLeft", "duration":"600"}'>
						<i class="fa fa-check"></i>
					</div>
					<div class="feature-info">
						<h4>Pelayanan Rawat Jalan</h4>
						<p>
							PELAYANAN RAWAT JALAN : 1. Konsultasi Gizi 2. Klinik VCT 3. Konsultasi Psikologi 4. Farmasi Klinik 5. Klinik KAB dan...
						</p>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="feature-box">
					<div class="feature-icon animate" data-animation-options='{"animation":"fadeInLeft", "duration":"600"}'>
						<i class="fa fa-check"></i>
					</div>
					<div class="feature-info">
						<h4>Poliklinik Kesehatan THT</h4>
						<p>
							Ini adalah contoh laman. Ini berbeda dengan posting blog karena akan tetap berada di satu tempat dan akan muncul di...
						</p>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="feature-box">
					<div class="feature-icon animate" data-animation-options='{"animation":"fadeInLeft", "duration":"600"}'>
						<i class="fa fa-check"></i>
					</div>
					<div class="feature-info">
						<h4>Poliklinik Spesialis</h4>
						<p>
							Rawat Jalan RSU Islam Klaten terpadu dengan Penunjang pelayanan lainnya sehingga akan memudahkan pelanggan mendapatkan pelayanan dalam satu atap. DOKTER...
						</p>
					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="feature-box">
					<div class="feature-icon animate" data-animation-options='{"animation":"fadeInLeft", "duration":"600"}'>
						<i class="fa fa-check"></i>
					</div>
					<div class="feature-info">
						<h4>Poliklinik Jantung</h4>
						<p>
							PELAYANAN JANTUNG TERPADU (Call Extension Pelayanan Jantung Terpadu : 348) Fasilitas : 1. Deteksi dini penyakit jantung : MCU (Medical...
						</p>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="feature-box">
					<div class="feature-icon animate" data-animation-options='{"animation":"fadeInLeft", "duration":"600"}'>
						<i class="fa fa-check"></i>
					</div>
					<div class="feature-info">
						<h4>Poliklinik Spesialis Kandungan</h4>
						<p>
							Ini adalah contoh laman. Ini berbeda dengan posting blog karena akan tetap berada di satu tempat dan akan muncul di...
						</p>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="feature-box">
					<div class="feature-icon animate" data-animation-options='{"animation":"fadeInLeft", "duration":"600"}'>
						<i class="fa fa-check"></i>
					</div>
					<div class="feature-info">
						<h4>Poliklinik Obsgyn/Kandungan</h4>
						<p>
							KLINIK INGIN ANAK (Call Extension Klinik Ingin Anak : 342) Fasilitas : 1. Penanganan oleh Konsultan Fertilitas Endokrinologi Reproduksi 2....
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="wrapper wrapper-grey">
	<!-- start: GENERIC CONTENT CONTAINER -->
	<div class="container">
		<div class="row">
			<div class="col-sm-6">
				<img src="<?php echo base_url();?>assets/frontend/images/gambar/rsi_klaten.jpg" class="img-responsive animate-if-visible" data-animation-options='{"animation":"tada", "duration":"600"}'>
			</div>
			<div class="col-sm-6">
				<h2>RSU Islam Boyolali</h2>
				<p>
					Assalamu’alaikum Wr. Wb.Puji Syukur kita panjatkan ke hadirat Allah SWT, atas rahmat dan karunia-Nya, sholawat dan salam senantiasa tercurah kepada Rasulullah SAW.Semoga Website ini dapat menjadi jembatan penghubung antar stakeholder. Dan dapat memberikan informasi yang...
				</p>
				<p>
					Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
				</p>
				<hr class="fade-right">
				<a class="btn btn-default" href="#"><i class="fa fa-info"></i> Baca selanjutnya...</a>
			</div>
		</div>
	</div>
</section>

<section class="wrapper wrapper-grey padding50">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2 class="center">Dokter</h2>
				<hr>

				<div class="flexslider" data-plugin-options='{"controlNav":false,"sync": "#carousel"}'>
				</div>
				<div id="carousel" class="flexslider" data-plugin-options='{"itemWidth": 300, "itemMargin": 5}'>
					<ul class="slides">
						<li>
							<div class="grid-item animate">
								<a href="#">
									<div class="grid-image">
										<img src="<?php echo base_url();?>assets/frontend/images/gambar/img1_syamsul.jpg" class="imgdokter img-responsive"/>
										<span class="image-overlay"> <i class="fa fa-mail-forward circle-icon circle-main-color"></i> </span>
									</div>
									<div class="grid-content">
										4 Columns Portfolio
									</div>
								</a>
							</div>
						</li>
						<li>
							<div class="grid-item animate">
								<a href="#">
									<div class="grid-image">
										<img src="<?php echo base_url();?>assets/frontend/images/gambar/img2_maria.jpg" class="imgdokter img-responsive"/>
										<span class="image-overlay"> <i class="fa fa-mail-forward circle-icon circle-main-color"></i> </span>
									</div>
									<div class="grid-content">
										3 Columns Portfolio
									</div>
								</a>
							</div>
						</li>
						<li>
							<div class="grid-item animate">
								<a href="#">
									<div class="grid-image">
										<img src="<?php echo base_url();?>assets/frontend/images/gambar/img3_sjabani.jpg" class="imgdokter img-responsive"/>
										<span class="image-overlay"> <i class="fa fa-mail-forward circle-icon circle-main-color"></i> </span>
									</div>
									<div class="grid-content">
										4 Columns Portfolio
									</div>
								</a>
							</div>
						</li>
						<li>
							<div class="grid-item animate">
								<a href="#">
									<div class="grid-image">
										<img src="<?php echo base_url();?>assets/frontend/images/gambar/img4_tribumi.jpg" class="imgdokter img-responsive"/>
										<span class="image-overlay"> <i class="fa fa-mail-forward circle-icon circle-main-color"></i> </span>
									</div>
									<div class="grid-content">
										2 Columns Portfolio
									</div>
								</a>
							</div>
						</li>
						<li>
							<div class="grid-item animate">
								<a href="#">
									<div class="grid-image">
										<img src="<?php echo base_url();?>assets/frontend/images/gambar/img5_ani.jpg" class="imgdokter img-responsive"/>
										<span class="image-overlay"> <i class="fa fa-mail-forward circle-icon circle-main-color"></i> </span>
									</div>
									<div class="grid-content">
										Single Project
									</div>
								</a>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>