<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$autoload['packages'] = array();

// $autoload['libraries'] = array('session', 'database','pagination', 'upload');
$autoload['libraries'] = array();

$autoload['drivers'] = array();

// $autoload['helper'] = array('url', 'form', 'file', 'text', 'security', 'pdf');
$autoload['helper'] = array('url');

$autoload['config'] = array();

$autoload['language'] = array();

$autoload['model'] = array();
